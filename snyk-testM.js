// crypto.createHmac() demo example

// Importing the crypto module
const crypto = require('crypto');

// Defining the secret key
const secret = 'TutorialsPoint';

const hmacValue = crypto.createHmac('sha256', secret)

   // Data to be encoded
   .update('Welcome to TutorialsPoint !')

   // Defining encoding type
   .digest('hex');
