// crypto.createHmac() demo example

// Importing the crypto module
// const crypto = require('crypto');

// Defining the secret key
// const secret = 'TutorialsPoint';

// const hmacValue = crypto.createHmac('sha256', 'pa02kdks9ak29aka9ka')
exports.hmac = data => crypto.createHmac('sha256', 'pa02kdks9ak29aka9ka')

   // Data to be encoded
   .update('Welcome to TutorialsPoint !')

   // Defining encoding type
   .digest('hex');
